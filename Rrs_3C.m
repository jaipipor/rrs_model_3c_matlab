function [Rrs,Rrs_mod,R_G,LT_Es_mod,xsol,epsilon]=Rrs_3C(theta_sun,wl,LT,Li,Es,optim_opts)
% [Rrs,Rrs_mod,R_G,LT_Es_mod,xsol,epsilon]=Rrs_3C(theta_sun,wl,LT,Li,Es,optim_opts)
% Matlab implementation of the 3C Rrs model, based on above-water measurements
% Input data:
%	� theta_sun:	sun zenith angle (single numerical value. Units: degrees)
%	� wl:			wavelengths (dimensions Nx1, Units: nanometer)
%	� LT:			upwelling radiance in air, referred to the wavelength vector (dimensions Nx1, Units: nanometer)
%	� Li:			sky radiance in air, referred to the wavelength vector (dimensions Nx1, Units: nanometer)
%	� Es:			downwelling irradiance in air, referred to the wavelength vector (dimensions Nx1, Units: nanometer)
%	� optim_opts:	structure, containing the following fields:
%						- generations: number of iterations of the optimization routine
%						- population: number of solutions tried at each generation
%						- lb: lower bounds for the optimizable parameters
%						- ini_pop: initial population for the optimizable parameters
%						- ub: upper bounds for the optimization parameters
%							The optimizable parameters are, in order:
%							(1) beta: aerosol turbidity coefficient at 550 nm (m^(-1))
%							(2) alpha: aerosol Ångström exponent
%							(3) C_a: chlorophyll concentration (mg m^(-3))
%							(3) C_TSM: total suspended matter concentation (g m^(-3))
%							(4) eta: backscattering spectral slope
%							(6) a_g: CDOM absorption at 440 nm (m^(-1))
%							(7) n_g: CDOM spectral slope (power-law model)
%							(8) rho: weight for the Li/Es term in the glint reflectance
%							(9) f_sd: weight for modeled contribution of direct irradiance fraction into the glint signal
%							(10) f_ss: weight for modeled contribution of diffuse irradiance fraction into the glint signal
%							(11) delta: constant glint term
%
% Output data:
%	� Rrs:				3C-optimal remote-sensing reflectance (Nx1. Units: sr^(-1))
%	� Rrs_mod:			modeled remote-sensing reflectance (Nx1. Units: sr^(-1))
%	� R_G:				glint reflectance, R_G=LT/Es-Rrs (Nx1. Units: sr^(-1))
%	� LT_Es_mod:		modeled upwelling radiance reflectance in air (Nx1. Units: sr^(-1))
%	� xsol:				optimized model parameters (12x1)
%	� epsilon:			optimized goal function (1x1)



%------------definition of some fixed parameters ----------------------------------
am=4;%atmospheric model (1-10)
rh=80;%relative humidity (%)
pressure=1013.25;%atmospheric pressure (mbar)
T_w=10;%water temperature (degrees)
n_w=1.34;%index of refraction of water

%------------loading external data in text files ----------------------------------
global base_folder theta_view
kk=load([base_folder,'data\aw_matlab.txt']);a_w=interp1(kk(:,1),kk(:,2),wl);
kk=load([base_folder,'data\daWdT_matlab.txt']);daw_dT=interp1(kk(:,1),kk(:,2),wl);
kk=load([base_folder,'data\astar_ph_matlab.txt']);astar_ph=interp1(kk(:,1),kk(:,2),wl);

%----------reflectance coefficients --------------------------
rho_sd=ref_fresnel(theta_sun);
rho_ss=0.06087+0.03751*(1-cosd(theta_sun))+0.1143*(1-cosd(theta_sun))^2;

%----------radiance normalization --------------------------
Li_Es=Li./Es;
LT_Es=LT./Es;

weights=optim_opts.weights;
lb=optim_opts.lb;
ini_pop=optim_opts.ini_pop;
ub=optim_opts.ub;


%----------preparation of the optimization --------------------------
nvars=length(ub);
generations=optim_opts.generations;
population=optim_opts.population;

optionsga = optimoptions('ga','Generations',generations,...
    'PopulationSize',population,'TolFun',0,'InitialPopulation',ini_pop,'StallTimeLimit',Inf,'StallGenLimit',Inf,'TimeLimit',Inf,'HybridFcn',@fmincon,'PlotFcn',@gaplotbestf);

%----------optimization running --------------------------
[xsol,epsilon] = ga(@(x) eval_model_LT_Es...
    (x,theta_sun,theta_view,am,rh,pressure,wl,a_w,daw_dT,astar_ph,T_w,n_w,Li_Es,LT_Es,weights,rho_sd,rho_ss),nvars,[],[],[],[],lb,ub,[],optionsga);

%---------- variable reconstruction--------------------------
[R_G,LT_Es_mod,Rrs_mod,Rrs]=reconst_3C...
    (xsol,theta_sun,theta_view,am,rh,pressure,wl,a_w,daw_dT,astar_ph,T_w,n_w,Li_Es,LT_Es,rho_sd,rho_ss);