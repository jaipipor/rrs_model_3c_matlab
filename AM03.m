function Rrs=AM03(wl,a_w, daw_dT, astar_ph, C_chl, C_mie, n_mie, C_y, n_y, T_w, theta_sun, theta_view, n_w)
%
% Rrs=AM03(wl,a_w, daw_dT, astar_ph, C_chl, C_mie, n_mie, C_y, n_y, T_w, theta_sun, theta_view, n_w)
% Albert and Mobley bio-optical model implementation. Modified by Jaime Pitarch. Changed CDOM model, from exponential to power law
% Input data:
%	· wl:			wavelength(nm)
%	· a_w:			absorption of pure water (m^(-1))
%	· daw_dT:		derivative of the pure water absorption coefficient respect to temperature (m^(-1)/ºC)
%	· astar_ph:		specific phytoplankton absorption coefficient (m^2/mg)
%	· C_chl:		chlorophyll concentration (mg m^(-3))
%	· C_mie:		total suspended matter concentration (g m^(-3))
%	· n_mie:		eta: backscattering spectral slope
%	· C_y:			CDOM absorption at 440 nm (m^(-1))
%	· n_y:			CDOM spectral slope (power-law model)
%	· T_w:			Water temperature (ºC)
%	· theta_sun:	sun zenith angle (degrees)
%	· theta_view:	view zenith angle (degrees)
%	· n_w:			Water index of refraction
%
% Output data:
%	· Rrs:			modeled remote-sensing reflectance (Nx1. Units: sr^(-1))


% calc_a_ph
a_ph = C_chl * astar_ph;
% calc_a_y
wl_ref_y = 440;
if n_y==-1,a_y = C_y * astar_y;else, a_y = C_y * (wl_ref_y./wl).^n_y;end
% calc_a
T_w_ref = 20.;
a_w_corr = a_w + (T_w - T_w_ref) .* daw_dT;
a = a_w_corr + a_ph + a_y;
% calc_bb_sm
bbstar_mie = 0.0042;
wl_ref_mie = 500;
bb_sm = C_mie * bbstar_mie * (wl / wl_ref_mie).^n_mie;
% calc_bb
if n_w==1.34,b1 = 0.00144;else b1 =  0.00111;end
wl_ref_water = 500;
S_water = -4.32;
bb_water = b1 * (wl / wl_ref_water).^S_water;
bb = bb_water + bb_sm;
% calc omega_b
omega_b = bb ./ (bb + a);
% calc sun and viewing zenith angles under water
theta_sun_ = theta_sun * pi / 180.;
theta_sun_ss = asin(sin(theta_sun_) / n_w);
theta_view_ = theta_view * pi / 180.;
theta_view_ss = asin(sin(theta_view_) / n_w);

p_f = [0.1034, 1, 3.3586, -6.5358, 4.6638, 2.4121];
p_frs = [0.0512, 1, 4.6659, -7.8387, 5.4571, 0.1098, 0.4021];

% calc subsurface reflectance
f = p_f(1) * (p_f(2) + p_f(3) * omega_b + p_f(4) * omega_b.^2 + p_f(5) * omega_b.^3) .* (1 + p_f(6) ./ cos(theta_sun_ss));
R0minus = f .* omega_b;
% calc subsurface remote sensing reflectance
frs = p_frs(1) * (p_frs(2) + p_frs(3) * omega_b + p_frs(4) * omega_b.^2 + p_frs(5) * omega_b.^3) .* (1 + p_frs(6) ./ cos(theta_sun_ss)) .* (1 + p_frs(7) ./ cos(theta_view_ss));
Rrs0minus = frs .* omega_b;
% calc_Rrs0plus (Lee1998, eq22), R=Q*Rrs
gamma = 0.48;
zeta = 0.518;
Rrs = zeta * Rrs0minus ./ ( 1 - gamma * R0minus );