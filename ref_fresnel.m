function rho=ref_fresnel(th)
% rho=ref_fresnel(th)
% Fresnel reflectance (rho) of unpolarized light given the zenithal angle. It works with vectors
% Input data:
%	ท th:	Incident light zenith angle (single numerical value. Units: degrees)
%
% Output data:
%	ท rho:	Fresnel reflectance factor (unitless)

rho=nan(size(th));
for i=1:length(th)
    my_th=th(i);
    my_thp=asind(sind(my_th)/1.34);
    rho(i)=1/2*abs(sind(my_th-my_thp).^2/sind(my_th+my_thp).^2+tand(my_th-my_thp).^2/tand(my_th+my_thp).^2);
end