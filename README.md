README 3C Water Surface Reflection Model

14 May 2020

Matlab implementation of an analytic remote sensing reflectance model as described & validated in Gege and Groetsch (2016), Groetsch et al. (2017), Groetsch et al. (2020), and Pitarch et al. (2020):

Gege, P., & Groetsch, P. (2016). A spectral model for correcting sun glint and sky glint. In Proceedings of Ocean Optics XXIII.

Groetsch, P. M. M., Gege, P., Simis, S. G. H., Eleveld, M. A., & Peters, S. W. M. (2017). Validation of a spectral correction procedure for sun and sky reflections in above-water reflectance measurements. Optics Express, 25(16), A742–A761.

Groetsch, P. M. M., Foster, R., & Gilerson, A. (2020). Exploring the limits for sky and sun glint correction of hyperspectral above-surface reflectance observations. Applied Optics, 59(9), 2942. 

Pitarch, J., Talone, M., Zibordi, G., & Groetsch, P. (2020). Determination of the remote-sensing reflectance from above-water measurements with the “3C model”: a further assessment. Optics Express, 28(11), 15885–15906. 

Please check on gitlab for the most recent version of this package:
https://gitlab.com/jaipipor/rrs_model_3c_matlab

Also check the gitlab of the original code in Python by Philipp Groetsch:
https://gitlab.com/pgroetsch/rrs_model_3C 

__author__ = "Jaime Pitarch"
__copyright__ = "Copyright 2019, Jaime Pitarch"
__license__ = "LGPL"
__version__ = "1.0"
__maintainer__ = "Jaime Pitarch"
__email__ = "jaipipor@msn.com"
__status__ = "Development"

1) Dependencies
Matlab(>=7.0)
Global Optimization Toolbox

2) Usage

-- External call --
The model is written in Matlab. A script "call_Rrs_3C.m" was prepared to run an example case. The script loads the needed input data, and runs the main function "Rrs_3C.m"

-- External call --
The Matlab function "Rrs_3C.m" performs the optimization:
[Rrs,Rrs_mod,R_G,Lu_Ed_mod,xsol,epsilon]=Rrs_3C(theta_sun,wl,LT,Li,Es,optim_opts)

See the help of the file "Rrs_3C.m" for explanations.