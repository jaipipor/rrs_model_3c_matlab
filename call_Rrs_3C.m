theta_sun=59.062; %sun zenith angle. In an operational context, this could be set as a function of geolocalization and time
global base_folder theta_view

base_folder='C:\Users\jaipi\Documents\MATLAB\3C_for_paper\';%please change this folder name with yours
kk=load([base_folder,'data\ex_data.txt']);
wl=kk(:,1);
LT=kk(:,2);
Li=kk(:,3);
Es=kk(:,4);
theta_view=35;%this parameter is the view zenith angle in degrees, and is assumed fixed for every used. For this reason, it is set here and not passed as parameter. Please modify it accordingly.
rho_s=ref_fresnel(theta_view);
%----------spectral weights for the goal function --------------------------
weights=ones(size(wl));
weights(wl>755&wl<775)=0;
%----------variables represented in order: beta, alpha, C_a, C_TSM, eta, C_g, n_g, f_sd, f_ss, delta--------------------------
lb=     [0.1  0.1  0.5  0.1   -2.5  0.05  5.5  0    0  -0.02   0];%lower bounds for minimization
ini_pop=[ 1   1.5  5     5     -1    1    5.8  rho_s  0.01  0.01   1e-3];%initial guess for minimization
ub=     [1.5  2.5  40    50    0     5    6.5  rho_s    1    1     2e-2];%uper bounds for minimization
optim_opts.weights=weights;
optim_opts.lb=lb;
optim_opts.ini_pop=ini_pop;
optim_opts.ub=ub;
optim_opts.generations=200;%number of generations (iterations)
optim_opts.population=80;%population of each generation (number of different solutions evaluated)


[Rrs,Rrs_mod,R_G,LT_Es_mod,xsol,epsilon]=Rrs_3C(theta_sun,wl,LT,Li,Es,optim_opts);%call to the 3C model
figure,plot(wl,[LT./Es LT_Es_mod R_G Rrs Rrs_mod],'linewidth',2),xlabel('\lambda (nm)'),ylabel('Reflectances (sr^{-1})'),legend('L_T/E_s','L_T/E_s mod','R_G','R_r_s','R_r_s mod'),grid on %plots