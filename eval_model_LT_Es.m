function epsilon=eval_model_LT_Es(x,theta_sun,theta_view,am,rh,pressure,wl,a_w,daw_dT,astar_ph,T_w,n_w,Li_Es,LT_Es,weights,rho_sd,rho_ss)
% epsilon=eval_model_LT_Es(x,theta_sun,theta_view,am,rh,pressure,wl,a_w,daw_dT,astar_ph,T_w,n_w,Li_Es,LT_Es,weights,rho_sd,rho_ss)
% This function evaluates the goal function based on the given parameter models
% Input data:
%	· x:			model parameters (12x1)
%	· theta_sun:	sun zenith angle (degrees)
%	· theta_view:	view zenith angle (degrees)
%	· am:			atmospheric model (1-10)
%	· rh:			relative humidity (%)
%	· pressure:		atmospheric pressure (mbar)
%	· wl:			wavelength(nm)
%	· a_w:			absorption of pure water (m^(-1))
%	· daw_dT:		Derivative of the pure water absorption coefficient respect to temperature (m^(-1)/ºC)
%	· astar_ph:		Specific phytoplankton absorption coefficient (m^2/mg)
%	· T:			Water temperature (ºC)
%	· n_w:			Water index of refraction
%	· Li_Es:		Ratio of sky radiance to downwelling irradiance in air (sr^(-1))
%	· LT_Es:		Ratio of upwelling radiance to downwelling irradiance in air (sr^(-1))
%	· weights:		spectral weights for the goal function
%	· rho_sd:		Surface reflectance factor of direct irradiance
%	· rho_ss:		Surface reflectance factor of diffuse irradiance
%
% Output data:
%	· epsilon:			Goal function (1x1)



[Esd_Es,Essr_Es,Essa_Es]=GC90(theta_sun, x(1), x(2), am, rh, pressure, wl);
Rrs_mod=AM03(wl,a_w, daw_dT, astar_ph, x(3), x(4), x(5), x(6), x(7), T_w, theta_sun, theta_view, n_w);
R_G = x(8) * Li_Es + x(9) * rho_sd*Esd_Es / pi + x(10) * rho_ss*(Essr_Es + Essa_Es) / pi + x(11);
LT_Es_mod = Rrs_mod + R_G;
epsilon=norm((LT_Es_mod-LT_Es).*weights);