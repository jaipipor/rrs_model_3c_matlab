function [Esd_Es,Essr_Es,Essa_Es]=GC90(theta_sun, beta, alpha, am, rh, pressure, wl)
% [Esd_Es,Essr_Es,Essa_Es]=GC90(theta_sun, beta, alpha, am, rh, pressure, wl)
% Matlab implementation of the Gregg and Carder model
% Input data:
%	· theta_sun:	sun zenith angle (degrees)
%	· beta:			aerosol turbidity coefficient at 550 nm (m^(-1))
%	· alpha:		aerosol Ångström exponent
%	· am:			atmospheric model (1-10)
%	· rh:			relative humidity (%)
%	· pressure:		atmospheric pressure (mbar)
%	· wl:			wavelength(nm)
% Output data:
%	· Esd_Es:			Direct irradiance ratio
%	· Essr_Es:			Diffuse irradiance ratio caused by Rayleigh scattering
%	· Essa_Es:			Diffuse irradiance ratio caused by aerosols

wl_a = 550;
theta_sun_ = theta_sun * pi / 180;
z3 = -0.1417 * alpha + 0.82;if alpha>1.2,z2 = 0.65;else, z2= z3;end;if alpha<0,z1 = 0.82;else, z1= z2;end;
theta_sun_mean = z1;
B3 = log(1 - theta_sun_mean);
B2 = B3 * (0.0783 + B3 * (-0.3824 - 0.5874 * B3));
B1 = B3 * (1.459 + B3 * (0.1595 + 0.4129 * B3));
Fa = 1 - 0.5 * exp((B1 + B2 * cos(theta_sun_)) * cos(theta_sun_));

omega_a = (-0.0032 * am + 0.972) * exp(3.06 * 1e-4 * rh);
tau_a = beta*(wl/wl_a).^(-alpha);
M  = 1 / (cos(theta_sun_) + 0.50572 * (90 + 6.07995 - theta_sun)^(-1.6364));
M_ = M * pressure / 1013.25;

Tr = exp(- M_ ./ (115.6406 * (wl / 1000).^4 - 1.335 * (wl / 1000).^2));
Tas = exp(- omega_a * tau_a * M);

Esd = Tr .* Tas;
Essr = 0.5 * (1 - Tr.^0.95);
Essa = Tr.^1.5 .* (1 - Tas) * Fa;

Es = Esd + Essr + Essa;
Esd_Es = Esd ./ Es;
Essr_Es = Essr ./ Es;
Essa_Es = Essa ./ Es;